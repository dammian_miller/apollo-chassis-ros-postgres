#include "koralROS/Detector.h"
#include "koralROS/Matcher.h"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <getopt.h>

#include <sstream>
#include <iostream>
#include <thread>    
#include <vector>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <netinet/in.h>

#include <city.h>

#include <limits>
#include <inttypes.h>

#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"

#include <morton-nd/mortonND_BMI2.h>
#include <morton-nd/mortonND_LUT_encoder.h>

using MortonND_8D = mortonnd::MortonNDBmi<8, uint64_t>;

// https://stackoverflow.com/questions/12219928/pack-unpack-short-into-int
int32_t Pack16_to_32(int16_t a, int16_t b)
{
	return (int32_t)( (uint32_t)a<<16 | (uint32_t)b );
}

int16_t UnpackA(int32_t x)
{
   return (int16_t)(((uint32_t)x)>>16);
}

int16_t UnpackB(int32_t x)
{
   return (int16_t)(((uint32_t)x)&0xffff);
}


// https://codereview.stackexchange.com/questions/80386/packing-and-unpacking-two-32-bit-integers-into-an-unsigned-64-bit-integer
uint64_t combine(uint32_t low, uint32_t high)
{
     return (((uint64_t) high) << 32) | ((uint64_t) low);
}

uint32_t high(uint64_t combined)
{
    return combined >> 32;
}

uint32_t low(uint64_t combined)
{
    uint64_t mask = std::numeric_limits<uint32_t>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

// https://stackoverflow.com/questions/22648978/c-how-to-find-the-length-of-an-integer
int int_length(int i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

int int64_length(int64_t i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

void uint64_to_string( uint64 value, std::string& result ) {
    result.clear();
    result.reserve( 20 ); // max. 20 digits possible
    uint64 q = value;
    do {
        result += "0123456789"[ q % 10 ];
        q /= 10;
    } while ( q );
    std::reverse( result.begin(), result.end() );
}

// https://stackoverflow.com/questions/9695720/how-do-i-convert-a-64bit-integer-to-a-char-array-and-back
void int64ToChar(char mesg[], int64_t num) {
    *(int64_t *)mesg = htonl(num);
}

// https://stackoverflow.com/questions/24538954/how-do-you-cast-a-uint64-to-an-int64
uint64_t Int64_2_UInt64(int64_t value)
{
     return (((uint64_t)((uint32_t)((uint64_t)value >> 32))) << 32) 
        | (uint64_t)((uint32_t)((uint64_t)value & 0x0ffffffff));           
}

int64_t UInt64_2_Int64(uint64_t value)
{
    return (int64_t)((((int64_t)(uint32_t)((uint64_t)value >> 32)) << 32) 
       | (int64_t)((uint32_t)((uint64_t)value & 0x0ffffffff)));           
}

int32_t uint32_to_int32(uint32_t value)
{
	int32_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return value;//tmp;
}

int64_t uint64_to_int64(uint64_t value)
{
	int64_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return tmp;
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

char *umyhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(uint16_t *)dest = htobe16(*(uint16_t *)src);
    break;
  case 4:
    *(uint32_t *)dest = htobe32(*(uint32_t *)src);
    break;
  case 8:
    *(uint64_t *)dest = htobe64(*(uint64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

// https://www.techiedelight.com/get-slice-sub-vector-from-vector-cpp/
template<typename T>
std::vector<T> vec_slice(std::vector<T> const &v, int s, int e)
{
	auto first = v.cbegin() + s;
	auto last = v.cbegin() + e;
	std::vector<T> vec(first, last);
	return vec;
}

class koralROS
{
public:
	apollo_chassis(std::string &topicName, std::string &db_conn_str_, char conninfo[], int &db_pool_size_) : thread_count(db_pool_size_)
	{
		int db_conn_count = db_pool_size_;
		pgbackend = std::make_shared<PGBackend>(db_conn_str_, db_conn_count);
		ros::master::V_TopicInfo topic_infos;
		ros::master::getTopics(topic_infos);
		for(int i, l = topic_infos.size(); i < l; i++) {
			if((topic_infos[i].name).find(s1topic) != std::string::npos)
			{
				type_hash = uint32_to_int32( CityHash32(topic_infos[i].datatype.c_str(), ((size_t)sizeof(topic_infos[i].datatype.size()))) );
			}
		}
		std::cout << "type_hash: " << type_hash << std::endl;
		// register callback
	}
	
	void compressedImageCallback(const sensor_msgs::CompressedImageConstPtr& img)	
	{
		// std::cout << "compressedImageCallback triggered" << std::endl;	
		cv_bridge::CvImagePtr imagePtr = cv_bridge::toCvCopy(img, color_encoding);

		cv_bridge::CvImage img_bridge;
		sensor_msgs::Image img_msg; // >> message to be sent

		std_msgs::Header header; // empty header
		header.seq = 1; // user defined counter
		header.stamp = ros::Time::now(); // time
		img_bridge = cv_bridge::CvImage(header, imagePtr->encoding, imagePtr->image.clone());
		img_bridge.toImageMsg(img_msg); // from cv_bridge to sensor_msgs::Image
		
		boost::shared_ptr<sensor_msgs::Image> comp_img_ptr;
		comp_img_ptr.reset(new sensor_msgs::Image(img_msg));
		imageCallback(comp_img_ptr);

	}

	void chassisCallback(const pb_msgs::Chassis& img)
	{
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
	
		char *frame_time;
		int frame_time_length = 0;
		// https://dba.stackexchange.com/questions/2796/how-do-i-get-the-current-unix-timestamp-from-postgresql/2798#2798
		// res = PQexec(conn, "select now()::text;");
		// if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		// 	std::cout << "Select failed: " << PQresultErrorMessage(res) << std::endl;
		// } else {
		// 	// http://www.cplusplus.com/reference/cstdlib/atoll/
		// 	frame_time = PQgetvalue(res, 0, 0);
		// 	frame_time_length = PQgetlength(res, 0, 0);
		// }
		// // std::cout << "frame_time length: " << std::to_string(PQgetlength(res, 0, 0)) << std::endl;
		// PQclear(res);

		std::vector<std::thread> threads(thread_count);
		// spawn n threads:

		for (int i = 0; i < thread_count; i++) {
			threads[i] = std::thread(boost::bind(&koralROS::persistDescriptors, this, _1, _2), desc_chunk, kps_chunk);
		}

		for (auto& th : threads) {
			th.join();
		}
		
		// WORKS

		high_resolution_clock::time_point t4 = high_resolution_clock::now();
		auto transaction_total_duration = duration_cast<milliseconds>( t4 - t1 ).count();
		// std::cout << "tuple construction duration: " << tuple_duration << std::endl;
		// std::cout << "transaction_run_duration: " << transaction_run_duration << std::endl;
		std::cout << "apollo_chassis transaction_total_duration: " << transaction_total_duration << std::endl;
		
	}

	cv::Mat image_with_kps;
	std::vector<cv::KeyPoint> kps;

	void persistDescriptors(std::vector<uint64_t> desc, std::vector<Keypoint> kps)
	{
		/* Binary COPY demo */
		char header[12] = "PGCOPY\n\377\r\n\0";
		int buf_pos = 0;
		int flag = 0;
		int extension = 0;
		int row_count = kps.size();
		// int buffer_size = (((row_count) * 126)+21); // v1...v8+mc
		int buffer_size = (((row_count) * 30)+21); // mc
		// std::cout << "buffer size on create: " << std::to_string(buffer_size) << std::endl;
		char buffer[buffer_size];
		int size = 0;
		buf_pos += 11;
		// std::cout << "buf_pos 1: " << std::to_string(buf_pos) << std::endl;
		memcpy(buffer, header, buf_pos);		
		memcpy(&buffer[buf_pos], (void *)&flag, 4);
		buf_pos += 4;
		// std::cout << "buf_pos 2: " << std::to_string(buf_pos) << std::endl;
		memcpy(&buffer[buf_pos], (void *)&extension, 4);
		buf_pos += 4;
		// std::cout << "buf_pos 3: " << std::to_string(buf_pos) << std::endl;
		
		short fieldnum = 0;
		int32_t id = 0;
		int64_t mc = 0;
		int64_t val = 0;
		int oid_length = 4;
		int oid = 0;
		int desc_idx = 0;
		char buff[21];

		for(int i = 0, l = row_count; i < l; i++) 
		{
			desc_idx = i * 8;
			// fieldnum = 11; // v1...v8+mc
			fieldnum = 3; // mc
			memcpy(&buffer[buf_pos], myhton((char *)&fieldnum, 2), 2);
			buf_pos += 2;
			// std::cout << "buf_pos 4: " << std::to_string(buf_pos) << std::endl;

			size = 8;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;
			mc = uint64_to_int64(MortonND_8D::Encode( desc.at(desc_idx+7), desc.at(desc_idx+6), desc.at(desc_idx+5), desc.at(desc_idx+4), desc.at(desc_idx+3), desc.at(desc_idx+2), desc.at(desc_idx+1), desc.at(desc_idx) ) );
			memcpy(&buffer[buf_pos], myhton((char *)&mc, 8), 8);
			buf_pos += 8;

			// type
			size = 4;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;

			memcpy(&buffer[buf_pos], &type_hash, 4);
			buf_pos += 4;

			// id: hash of x + y
			size = 4;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;
			// std::cout << "buf_pos 5: " << std::to_string(buf_pos) << std::endl;
			id = Pack16_to_32(kps.at(i).x, kps.at(i).y);
			// std::cout << "ID x: " << std::to_string(kps.at(i).x) << " ID y: " << std::to_string(kps.at(i).y) << " ID: " << std::to_string(id) <<  std::endl;
			memcpy(&buffer[buf_pos], myhton((char *)&id, 4), 4);
			buf_pos += 4;
			// std::cout << "buf_pos 6: " << std::to_string(buf_pos) << std::endl;

			// // Start values v1, v2...
			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+1));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+2));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+3));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+4));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+5));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+6));
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;
			
			// size = 8;
			// memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			// buf_pos += 4;
			// // std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
			// val = uint64_to_int64(desc.at(desc_idx+7));			
			// memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
			// buf_pos += 8;
			// // std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;
			

		}

		// std::cout << "buf_pos 6: " << std::to_string(buf_pos) << std::endl;

		// end of file
		short negative = -1;
		memcpy(&buffer[buf_pos], myhton((char *)&negative, 2), 2);
		// std::cout << "buffer size: " << std::to_string(sizeof(buffer)) << std::endl;
		
		// high_resolution_clock::time_point t2 = high_resolution_clock::now();
   		// auto tuple_duration = duration_cast<milliseconds>( t2 - t1 ).count();
		// PGconn *conn = pgbackend->connection()->connection().get();
		auto conn = pgbackend->connection();
		PGresult *res = NULL;

		// char *frame_time;
		// int frame_time_length = 0;
		// res = PQexec(conn->connection().get(), "select now()::text;");
		// if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		// 	std::cout << "Select failed: " << PQresultErrorMessage(res) << std::endl;
		// } else {
		// 	// http://www.cplusplus.com/reference/cstdlib/atoll/
		// 	frame_time = PQgetvalue(res, 0, 0);
		// 	frame_time_length = PQgetlength(res, 0, 0);
		// }
		// std::cout << "frame_time length: " << std::to_string(PQgetlength(res, 0, 0)) << std::endl;
		// PQclear(res);

		// res = PQexec(conn->connection().get(), "COPY mem_512_l0 (t, i, v1, v2, v3, v4, v5, v6, v7, v8, mc) FROM STDIN (FORMAT binary);"); // v1...v8+mc
		res = PQexec(conn->connection().get(), "COPY mem_l0 (v, t, i) FROM STDIN (FORMAT binary);"); // mc
		if (PQresultStatus(res) != PGRES_COPY_IN)
		{
			std::cout << "Not in COPY_IN mode";
			PQclear(res);
		}
		else
		{
			PQclear(res);
			// std::cout << "Enter COPY_IN mode" << std::endl;
			int copyRes = PQputCopyData(conn->connection().get(), buffer, buffer_size);
			if (copyRes == 1)
			{
				if (PQputCopyEnd(conn->connection().get(), NULL) == 1)
				{
					res = PQgetResult(conn->connection().get());
					if (PQresultStatus(res) != PGRES_COMMAND_OK)
					{
						std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
					}
					PQclear(res);
				}
				else
				{
					std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
				}
			}
			else if (copyRes == 0)
			{
				std::cout << "Send no data, connection is in nonblocking mode" << std::endl;
			}
			else if (copyRes == -1)
			{
				std::cout << "Error occur: " << PQerrorMessage(conn->connection().get()) << std::endl;
			}
		}
		pgbackend->freeConnection(conn);
	}

private:

	int thread_count = 0;

	int32_t type_hash = 0;

	std::shared_ptr<PGBackend> pgbackend;

	const std::string RAW = "raw";
	const std::string COMPRESSED = "compressed";

	std::string color_encoding = sensor_msgs::image_encodings::BGR8;

	ros::NodeHandle node;
	FeatureDetector &detector;
	FeatureMatcher &matcher;

	boost::shared_ptr<message_filters::Subscriber<sensor_msgs::CompressedImage>> comp_image_sub_ptr;
	boost::shared_ptr<message_filters::Subscriber<sensor_msgs::Image>> image_sub_ptr;

};

int main(int argc, char **argv)
{
	int thread_count = 4;
	int opt = 0;
	unsigned int width = 1280;
	unsigned int height = 720;
	unsigned int maxFeatureCount = 5000;
	uint8_t fastThreshold = 40;
	uint8_t matchThreshold = 25;
	unsigned int scaleLevels = 8;
	float scaleFactor = 1.2;

	std::string topicName = "/apollo/sensor/camera/traffic/image_short/compressed"; 
	std::string cameraTransport = "compressed"; // unused currently
	std::string color_encoding = "BGR8";

	std::string postgresHostIP = "";
	std::string postgresPort = "6432";
	std::string postgresDBName = "postgres";
	std::string postgresUser = "postgres";
	std::string postgresPassword = "password";

	bool displayFeatures = false;

	std::string container_id = "";

	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"width", 1, 0, '0'},
		{"height", 1, 0, '1'},
		{"maxFeatureCount", 1, 0, '2'},
		{"fastThreshold", 1, 0, '3'},
		{"matchThreshold", 1, 0, '4'},
		{"scaleLevels", 1, 0, '5'},
		{"scaleFactor", 1, 0, '6'},
		{"topicName", 1, 0, '7'},
		{"cameraTransport", 1, 0, '8'},
		{"postgresHostIP", 1, 0, '9'},
		{"postgresPort", 1, 0, 'a'},
		{"postgresDBName", 1, 0, 'b'},
		{"postgresUser", 1, 0, 'c'},
		{"postgresPassword", 1, 0, 'd'},
		{"displayFeatures", 1, 0, 'e'},
		{"containerId", 1, 0, 'f'},
		{"thread_count", 1, 0, 'g'},
		{"color_encoding", 1, 0, 'h'},
		{0, 0, 0, 0}}; 

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case '3':
			std::cout << "setting fastThreshold: " << optarg << std::endl;
			fastThreshold = atoi(optarg);
			break;
		case '4':
			std::cout << "setting matchThreshold: " << optarg << std::endl;
			matchThreshold = atoi(optarg);
			break;
		case '2':
			std::cout << "setting maxFeatureCount: " << optarg << std::endl;
			maxFeatureCount = strtoul(optarg, NULL, 0);
			break;
		case '5':
			std::cout << "setting scaleLevels: " << optarg << std::endl;
			scaleLevels = atoi(optarg);
			break;
		case '6':
			std::cout << "setting scaleFactor: " << optarg << std::endl;
			scaleFactor = atof(optarg);
			break;
		case '0':
			std::cout << "setting width: " << optarg << std::endl;
			width = atoi(optarg);
			break;
		case '1':
			std::cout << "setting height: " << optarg << std::endl;
			height = atoi(optarg);
			break;
		case '7':
			std::cout << "setting topicName: " << optarg << std::endl;
			topicName = optarg;
			break;
		case '8':
			std::cout << "setting cameraTransport: " << optarg << std::endl;
			cameraTransport = optarg;
			break;
		case '9':
			std::cout << "setting postgresHostIP: " << optarg << std::endl;
			postgresHostIP = optarg;
			break;
		case 'a':
			std::cout << "setting postgresPort: " << optarg << std::endl;
			postgresPort = optarg;
			break;
		case 'b':
			std::cout << "setting postgresDBName: " << optarg << std::endl;
			postgresDBName = optarg;
			break;
		case 'c':
			std::cout << "setting postgresUser: " << optarg << std::endl;
			postgresUser = optarg;
			break;
		case 'd':
			std::cout << "setting postgresPassword: " << optarg << std::endl;
			postgresPassword = optarg;
			break;
		case 'e':
			std::cout << "setting displayFeatures: " << optarg << std::endl;
			displayFeatures = !(std::string(optarg).compare("true")) ? true : false;
			break;
		case 'f':
			std::cout << "setting container id: " << optarg << std::endl;
			container_id = optarg;
			break;
		case 'g':
			std::cout << "setting thread count: " << optarg << std::endl;
			thread_count = atoi(optarg);
			break;
		case 'h':
			std::cout << "setting color_encoding: " << optarg << std::endl;
			color_encoding = optarg; // update to enum
			break;
		default: 
			exit(EXIT_FAILURE);
		}
	}

	std::cout << "Starting..." << std::endl;

	std::string ros_node_name = "koralROS_" + container_id;

	ros::init(argc, argv, ros_node_name);
	ros::NodeHandle nh;

	ros::master::V_TopicInfo topic_infos;
  	ros::master::getTopics(topic_infos);
	std::cout << "ROS topics: " << std::endl;
	for(int i, l = topic_infos.size(); i < l; i++) {
		std::cout << "topic name: " << topic_infos[i].name << " topic type: " << topic_infos[i].datatype << std::endl;
	}
	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgresDBName + "' user='" + postgresUser + "' password='" + postgresPassword + "'";
	// only if host IP specified then switch to TCP
	if(postgresHostIP.length() > 0)
	{
		db_conn_str = "postgresql://" + postgresUser + ":" + postgresPassword + "@" + postgresHostIP + ":" + postgresPort + "/" + postgresDBName;
	}
	std::cout << db_conn_str << std::endl;
	char conninfo[db_conn_str.size() + 1];
	strcpy(conninfo, db_conn_str.c_str());
	
	FeatureDetector detector(scaleFactor, scaleLevels, width, height, maxFeatureCount, fastThreshold);
	FeatureMatcher matcher(matchThreshold, maxFeatureCount);

	koralROS koral(detector, matcher, topicName, cameraTransport, color_encoding, db_conn_str, conninfo, thread_count);
	
	while (ros::ok())
	{
		ros::spinOnce();
   		if (displayFeatures && detector.receivedImg)
		{
			detector.converted_kps.clear();
			cv::namedWindow("Keypoints-"+container_id, CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
			cv::resizeWindow("Keypoints-"+container_id, 1280,720);
			cv::imshow("Keypoints-"+container_id, koral.image_with_kps.clone());
			cv::waitKey(1);
			detector.receivedImg = false;
		}
	}
	cv::destroyAllWindows();
	detector.freeGPUMemory();
	matcher.freeGPUMemory();
	cudaDeviceReset();
	return 0;
}
